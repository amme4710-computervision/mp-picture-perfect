clear
clc

neg_img_files = dir('./SMILEsmileD/SMILEs/negatives/negatives7/*.jpg'); 
pos_img_files = dir('./SMILEsmileD/SMILEs/positives/positives7/*.jpg');
posfiles = length(pos_img_files);    % Number of files found
negfiles = length(neg_img_files);    % Number of files found

images = {};
labels = [];

for i=1:posfiles
   currentfilename = pos_img_files(i).name;
   currentimage = imread("./SMILEsmileD/SMILEs/positives/positives7/" + currentfilename);
   pos_images{i} = currentimage;
   pos_labels(i) = 1;
end

for i=1:negfiles
   currentfilename = neg_img_files(i).name;
   currentimage = imread("./SMILEsmileD/SMILEs/negatives/negatives7/" + currentfilename);
   neg_images{i} = currentimage;
   neg_labels(i) = 0;
end

train_images = [pos_images(1:3000) neg_images(1:7000)];
test_images = [pos_images(3001:end) neg_images(7001:end)];
train_labels = [pos_labels(1:3000) neg_labels(1:7000)];
test_labels = [pos_labels(3001:end) neg_labels(7001:end)];
disp(length(train_images));
disp(length(test_labels));


train_features = saveFeatures(train_images);
svm_model = fitcecoc(train_features,train_labels);
% model = load('model.mat');
% svm_model = model.svm_model;
test_features = saveFeatures(test_images);

correct = 0;
total = 0;

for i = 1:length(test_labels)
    results = predict(svm_model, test_features(i,:));
    if results == test_labels(i)
        correct = correct + 1;
    end
    total = total + 1;
end

accuracy = (correct/total)*100
        
        


function features = saveFeatures(imgs)
    
    % initialise features matrix
    HOG_matrix = [];
    
    % get number of images
    num = length(imgs);
    
    % loop through images
    for i = 1:num
        
        % get current image
        img = cell2mat(imgs(i));

        % get HOG features
        hog_features = extractHOGFeatures(im2gray(uint8(img)));
        
        HOG_matrix = [HOG_matrix; hog_features];

    end
    
    features = HOG_matrix;
end
