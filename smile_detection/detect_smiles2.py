import cv2
import numpy as np
import os
from skimage import feature
from matplotlib import pyplot as plt
from skimage import color
from skimage.feature import hog
from sklearn import svm
from sklearn.metrics import classification_report,accuracy_score
import pickle
import albumentations as A

    

posfolder = "/home/evan/SMILEsmileD/SMILEs/positives/positives7" 
negfolder = "/home/evan/SMILEsmileD/SMILEs/negatives/negatives7" 

images = []
labels = []

# load in positive images and create list of labels
for filename in os.listdir(posfolder):
    if filename.endswith(".jpg"):
        img = cv2.imread(os.path.join(posfolder,filename))
        print(img.shape)
        images.append(img)
        labels.append(1)

# load in negative images and create list of labels
for filename in os.listdir(negfolder):
    if filename.endswith(".jpg"):
        img = cv2.imread(os.path.join(negfolder,filename))
        images.append(img)
        print(img.shape)
        labels.append(0)

labels = np.array(labels)

labels =  np.array(labels.reshape(len(labels),1))

print(labels.shape)
transform  = A.Compose([
                A.Rotate(limit=45, border_mode=0, value=0, p=0.5)
            ])
ppc = 16
hog_images = []
hog_features = []
for image in images:

    image = transform(image=image)["image"]

    # get HOG features
    fd, hog_image = hog(image, orientations=9, 
                        pixels_per_cell=(8, 8), cells_per_block=(2, 2), 
                        block_norm='L2-Hys', visualize=True)

    # fd,hog_image = hog(image, orientations=8, pixels_per_cell=(ppc,ppc),cells_per_block=(4, 4),block_norm= 'L2',visualize=True)
    hog_images.append(hog_image)
    hog_features.append(fd)


clf = svm.SVC()
hog_features = np.array(hog_features)
data_frame = np.hstack((hog_features,labels))
np.random.shuffle(data_frame)



#What percentage of data you want to keep for training
percentage = 100
partition = int(len(hog_features)*percentage/100)

x_train, x_test = data_frame[:partition,:-1],  data_frame[partition:,:-1]
y_train, y_test = data_frame[:partition,-1:].ravel() , data_frame[partition:,-1:].ravel()

clf.fit(x_train,y_train)

pickle.dump(clf, open('trained_svm_rot_hog.sav', 'wb'))

if percentage != 100:
    y_pred = clf.predict(x_test)

    print("Accuracy: "+str(accuracy_score(y_test, y_pred)))
    print('\n')
    print(classification_report(y_test, y_pred))

