import torchvision
import argparse
# import albumentations.pytorch.transforms as APy
import numpy as np 
import matplotlib.pyplot as plt

import os
from os.path import abspath, expanduser
from typing import Any, Callable, List, Dict, Optional, Tuple, Union

import torch

import torchvision.transforms as transforms

from torchvision.datasets.utils import (
    download_file_from_google_drive,
    download_and_extract_archive,
    extract_archive,
    verify_str_arg,
)
from torchvision.datasets.vision import VisionDataset


class WIDERToYOLO(VisionDataset):
    """`WIDERFace <http://shuoyang1213.me/WIDERFACE/>`_ Dataset.
    Args:
        root (string): Root directory where images and annotations are downloaded to.
            Expects the following folder structure if download=False:
            .. code::
                <root>
                    └── widerface
                        ├── wider_face_split ('wider_face_split.zip' if compressed)
                        ├── WIDER_train ('WIDER_train.zip' if compressed)
                        ├── WIDER_val ('WIDER_val.zip' if compressed)
                        └── WIDER_test ('WIDER_test.zip' if compressed)
        split (string): The dataset split to use. One of {``train``, ``val``, ``test``}.
            Defaults to ``train``.
        transform (callable, optional): A function/transform that  takes in a PIL image
            and returns a transformed version. E.g, ``transforms.RandomCrop``
        target_transform (callable, optional): A function/transform that takes in the
            target and transforms it.
        download (bool, optional): If true, downloads the dataset from the internet and
            puts it in root directory. If dataset is already downloaded, it is not
            downloaded again.
    """

    BASE_FOLDER = "widerface"
    FILE_LIST = [
        # File ID                             MD5 Hash                            Filename
        ("15hGDLhsx8bLgLcIRD5DhYt5iBxnjNF1M", "3fedf70df600953d25982bcd13d91ba2", "WIDER_train.zip"),
        ("1GUCogbp16PMGa39thoMMeWxp7Rp5oM8Q", "dfa7d7e790efa35df3788964cf0bbaea", "WIDER_val.zip"),
        ("1HIfDbVEWKmsYKJZm4lchTBDLW5N7dY5T", "e5d8f4248ed24c334bbd12f49c29dd40", "WIDER_test.zip"),
    ]
    ANNOTATIONS_FILE = (
        "http://shuoyang1213.me/WIDERFACE/support/bbx_annotation/wider_face_split.zip",
        "0e3767bcf0e326556d407bf5bff5d27c",
        "wider_face_split.zip",
    )

    def __init__(
        self,
        root: str,
        split: str = "train",
        transform: Optional[Callable] = None,
        target_transform: Optional[Callable] = None,
        download: bool = False,
        text_base_dir: str = '/content/drive/MyDrive/widerface/',
        verbose: bool = False
    ) -> None:
        super(WIDERToYOLO, self).__init__(
            root=os.path.join(root, self.BASE_FOLDER), transform=transform, target_transform=target_transform
        )

        self.txt_base_dir = text_base_dir
        self.debug = False
        self.verbose = verbose

        # check arguments
        self.split = verify_str_arg(split, "split", ("train", "val", "test"))

        if download:
            self.download()

        if not self._check_integrity():
            raise RuntimeError(
                "Dataset not found or corrupted. " + "You can use download=True to download and prepare it"
            )

        self.img_info: List[Dict[str, Union[str, Dict[str, torch.Tensor]]]] = []
        if self.split in ("train", "val"):
            self.parse_train_val_annotations_file()
        else:
            self.parse_test_annotations_file()

    def __getitem__(self, index: int) -> Tuple[Any, Any]:
        """
        Args:
            index (int): Index
        Returns:
            tuple: (image, target) where target is a dict of annotations for all faces in the image.
            target=None for the test split.
        """
        # stay consistent with other datasets and return a PIL Image
        # img = np.array(Image.open(self.img_info[index]["img_path"]))

        # use plt instead as it reads in image as a np.array by default
        img = plt.imread(self.img_info[index]["img_path"])
        height,width = img.shape[:2]

        # print(height,width)
        # extract annotations if dataset is not test
        target = None if self.split == "test" else self.img_info[index]["annotations"]

        # extract bboxes for augmentation
        bboxes = np.array(target['bbox']).tolist()

        if self.debug:
            print("-------------------------------------------------------------")
            print('\033[92m'+ '[DEBUG] bbox after np.array.tolist(): '+ '\033[0m', bboxes)

        bboxes = list(filter(lambda bbox: bbox[2] >= 5 and bbox[3] >= 5,
                          bboxes))

        if self.debug:
            print("-------------------------------------------------------------")
            print('\033[92m'+ '[DEBUG] bbox after adding filtering: '+  '\033[0m', bboxes)
        
        # spit path and image filename
        path, file = os.path.split(self.img_info[index]["img_path"])

        # split filename into extension 
        filename, ext = os.path.splitext(file)
        
        if self.verbose:
            print('\033[92m'+'Generating: '+  '\033[0m' + self.txt_base_dir + self.split + '/' + filename + '.txt')
        
        if not os.path.exists(self.txt_base_dir + self.split + '/'):
             os.makedirs(self.txt_base_dir + self.split + '/')

        # open file to write annotation to
        f = open(self.txt_base_dir + self.split + '/' + filename + '.txt', "w")

        for bbox in bboxes:

            # get bbox values
            x,y,w,h = bbox

            # get centre of bbox
            cX = x + w/2
            cY = y + h/2 

            # normalise centre, height and width
            cX_norm = cX/width
            cY_norm = cY/height
            w_norm = w/width
            h_norm = h/height

            # create string array for YOLO format bbox
            string_arr = ['0', str(float(cX_norm)), str(float(cY_norm)), str(float(w_norm)), str(float(h_norm)) ]

            annoataion = " ".join(string_arr)
            f.write(annoataion+'\n')

        f.close()

    def toggle_debug(self):
        self.debug = not self.debug

    def __len__(self) -> int:
        return len(self.img_info)

    def extra_repr(self) -> str:
        lines = ["Split: {split}"]
        return "\n".join(lines).format(**self.__dict__)

    def parse_train_val_annotations_file(self) -> None:
        filename = "wider_face_train_bbx_gt.txt" if self.split == "train" else "wider_face_val_bbx_gt.txt"
        filepath = os.path.join(self.root, "wider_face_split", filename)

        with open(filepath, "r") as f:
            lines = f.readlines()
            file_name_line, num_boxes_line, box_annotation_line = True, False, False
            num_boxes, box_counter = 0, 0
            labels = []
            for line in lines:
                line = line.rstrip()
                if file_name_line:
                    img_path = os.path.join(self.root, "WIDER_" + self.split, "images", line)
                    img_path = abspath(expanduser(img_path))
                    file_name_line = False
                    num_boxes_line = True
                elif num_boxes_line:
                    num_boxes = int(line)
                    num_boxes_line = False
                    box_annotation_line = True
                elif box_annotation_line:
                    box_counter += 1
                    line_split = line.split(" ")
                    line_values = [int(x) for x in line_split]
                    labels.append(line_values)
                    if box_counter >= num_boxes:
                        box_annotation_line = False
                        file_name_line = True
                        labels_tensor = torch.tensor(labels)
                        self.img_info.append(
                            {
                                "img_path": img_path,
                                "annotations": {
                                    "bbox": labels_tensor[:, 0:4],  # x, y, width, height
                                    "blur": labels_tensor[:, 4],
                                    "expression": labels_tensor[:, 5],
                                    "illumination": labels_tensor[:, 6],
                                    "occlusion": labels_tensor[:, 7],
                                    "pose": labels_tensor[:, 8],
                                    "invalid": labels_tensor[:, 9],
                                },
                            }
                        )
                        box_counter = 0
                        labels.clear()

                else:
                    raise RuntimeError("Error parsing annotation file {}".format(filepath))

    def parse_test_annotations_file(self) -> None:
        filepath = os.path.join(self.root, "wider_face_split", "wider_face_test_filelist.txt")
        filepath = abspath(expanduser(filepath))
        with open(filepath, "r") as f:
            lines = f.readlines()
            for line in lines:
                line = line.rstrip()
                img_path = os.path.join(self.root, "WIDER_test", "images", line)
                img_path = abspath(expanduser(img_path))
                self.img_info.append({"img_path": img_path})

    def _check_integrity(self) -> bool:
        # Allow original archive to be deleted (zip). Only need the extracted images
        all_files = self.FILE_LIST.copy()
        all_files.append(self.ANNOTATIONS_FILE)
        for (_, md5, filename) in all_files:
            file, ext = os.path.splitext(filename)
            extracted_dir = os.path.join(self.root, file)
            if not os.path.exists(extracted_dir):
                return False
        return True

    def download(self) -> None:
        if self._check_integrity():
            print("Files already downloaded and verified")
            return
        
        # download and extract image data
        for (file_id, md5, filename) in self.FILE_LIST:
            download_file_from_google_drive(file_id, self.root, filename, md5)
            filepath = os.path.join(self.root, filename)
            extract_archive(filepath)

        # download and extract annotation files
        download_and_extract_archive(
            url=self.ANNOTATIONS_FILE[0], download_root=self.root, md5=self.ANNOTATIONS_FILE[1]
        )


def detection_collate(batch):
    """Custom collate fn for dealing with batches of images that have a different
    number of associated object annotations (bounding boxes).

    Arguments:
        batch: (tuple) A tuple of tensor images and (lists of annotations, masks)

    Return:
        A tuple containing:
            1) (tensor) batch of images stacked on their 0 dim
            2) (list<tensor>, list<tensor>, list<int>) annotations for a given image are stacked
                on 0 dim. The output gt is a tuple of annotations and masks.
    """
    targets = list()
    imgs = list()
    labels = list() 
    # masks = []
    # num_crowds = []

    for sample in batch:
        imgs.append(sample[0])
        targets.append(torch.FloatTensor(sample[1]['bbox']))
        labels.append(torch.IntTensor(sample[2]))
        # masks.append(torch.FloatTensor(sample[1][1]))
        # num_crowds.append(sample[1][2])

    imgs_stacked = torch.stack(imgs, dim=0)


    return imgs_stacked, targets, labels

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--root', type=str, default='/content/datasets/', 
                        help='Root directory of wider faces dataset (<root>/widerfaces/WIDER_train ... )')
    parser.add_argument('--split', type=str, default='train', 
                        help='train or val')
    parser.add_argument('--text-base-dir', type=str, default='/content/drive/MyDrive/widerface/', 
                        help='Base directory to output .txt annotation files to. Where /<Base>/<split>/<.txt files>')
    parser.add_argument('--download', type=bool, default=False, 
                        help='True or False')
    parser.add_argument('--verbose', type=bool, default=False, 
                        help='True or False, shows name of textfile being generated')
    parser.add_argument('--silent', type=bool, default=False, 
                        help='True or False, silences progress bar output')
    args = parser.parse_args()

    data = WIDERToYOLO(root=args.root, split=args.split, text_base_dir=args.text_base_dir, download=args.download)

    print('\033[96m' + 'Generating ' + '\033[0m'+ \
          '\033[93m' + args.split + '\033[0m' + \
          '\033[96m' + ' Annotaions in: ' + '\033[0m'+ \
          '\033[93m' + args.text_base_dir + '\033[0m')

    
    for i in range(len(data)):
        if not args.silent:
            percentage = int((i/(len(data)-1))*100)
            if percentage == 100:
                arrow_pos = percentage//2-1
                progress_bar = list("=" * arrow_pos)
                progress_bar[-3:] = "Done"
                progress_bar = "".join(progress_bar)
                # print(progress_bar)
            else:
                arrow_pos = percentage//2
                progress_bar = list("=" * arrow_pos + "-" * (50-arrow_pos))
                progress_bar[arrow_pos] = ">"
                progress_bar = "".join(progress_bar)
                # print(progress_bar, end='\r')
            format_str = "{}/{}  ({}%)  [{}]".format(i+1,len(data),percentage,progress_bar)
            print('\r'+format_str, end='')

        data[i]

    print()
    print('\033[96m' + 'Generated ' + '\033[0m'+ \
          '\033[93m' + str(len(data)) + '\033[0m' + \
          '\033[96m' + ' Annotaions in: ' + '\033[0m'+ \
          '\033[93m' + args.text_base_dir + '\033[0m')