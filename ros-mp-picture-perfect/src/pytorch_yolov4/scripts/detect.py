#!/usr/bin/env python3

import argparse
import os
import platform
import shutil
import time
from pathlib import Path
import rospy
from skimage.feature import hog
from skimage import feature 

import pickle

import numpy as np

from sensor_msgs.msg import Image as rosImage
from std_msgs.msg import String as rosString
from pytorch_yolov4.np2ros import *
from pytorch_yolov4.msg import BoundingBox, BoundingBoxArray

import cv2
import torch
import torch.backends.cudnn as cudnn
from numpy import random

from utils.google_utils import attempt_load
from utils.datasets import LoadStreams, LoadImages
from utils.general import (
    check_img_size, non_max_suppression, apply_classifier, scale_coords, xyxy2xywh, strip_optimizer)
from utils.plots import plot_one_box
from utils.torch_utils import select_device, load_classifier, time_synchronized

from models.models import *
from utils.datasets import *
from utils.general import *

# for smile detection
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
import imutils

def getLBP(image, eps=1e-7):
    # compute the Local Binary Pattern representation
    # of the image, and then use the LBP representation
    # to build the histogram of patterns
    num_features = 48
    lbp = feature.local_binary_pattern(image, num_features, 8, method="uniform")
    (hist, _) = np.histogram(lbp.ravel(),
        bins=np.arange(0, num_features + 3),
        range=(0, num_features + 2))
    # normalize the histogram
    hist = hist.astype("float")
    hist /= (hist.sum() + eps)
    # return the histogram of Local Binary Patterns
    return hist


def detect_smile(img, svm_model):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    resized = cv2.resize(gray, (64,64), interpolation = cv2.INTER_AREA)
    features = []

    fd, _ = hog(resized, orientations=9, 
                        pixels_per_cell=(8, 8), cells_per_block=(2, 2), 
                        block_norm='L2-Hys', visualize=True)
    # print(fd)

    # LBP = getLBP(image=gray)
    # features.append([*fd, *LBP])
    features.append(fd)
    pred = svm_model.predict(np.array(features))

    if pred == 1:
        return 'smile'
    elif pred == 0:
        return 'no smile'
    else:
        return 'nicky fkd up'

def detect_smile_lenet(img, lenet_model):
    
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    roi = cv2.resize(gray, (28, 28))
    roi = roi.astype("float") / 255.0
    roi = img_to_array(roi)
    roi = np.expand_dims(roi, axis=0)


    # determine the probabilities of both "smiling" and "not
    # smiling", then set the label accordingly
    (notSmiling, smiling) = lenet_model.predict(roi)[0]
    label = "smile" if smiling > notSmiling else "no smile"

    return label


def detect_smile_orbhog(img, svm_model, BOW):
    ppc = 16
    features = []
    i = 0

    orb = cv2.ORB_create(nfeatures=1000, scoreType=cv2.ORB_FAST_SCORE)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    resized = cv2.resize(gray, (64,64), interpolation = cv2.INTER_AREA)
    # for image in images:
        # get HOG features
    fd, hog_image = hog(resized, orientations=9, 
                        pixels_per_cell=(8, 8), cells_per_block=(2, 2), 
                        block_norm='L2-Hys', visualize=True)

    # get ORB features
    resized = cv2.resize(gray, (128,128))

    kernel = np.array([[-1,-1,-1], [-1,9,-1], [-1,-1,-1]])
    sharpened = cv2.filter2D(resized, -1, kernel)

    kp, des = orb.detectAndCompute(sharpened, None)

    while len(kp) < 1:
        sharpened = cv2.filter2D(sharpened, -1, kernel)
        kp, des = orb.detectAndCompute(sharpened, None)

    histo = np.zeros(500)
    nkp = np.size(kp)

    for d in des:
        idx = BOW.predict([d])
        histo[idx] += 1/nkp # Because we need normalized histograms, I prefere to add 1/nkp directly

    features.append([*fd, *histo])
    # print(np.array([fd]).shape)

    pred = svm_model.predict(np.array(features))
        # features.append([histo])

    if pred == 1:
        return 'smile'
    elif pred == 0:
        return 'no smile'
    else:
        return 'nicky fkd up'

def load_classes(path):
    # Loads *.names file at 'path'
    with open(path, 'r') as f:
        names = f.read().split('\n')
    return list(filter(None, names))  # filter removes empty strings (such as last line)

def detect(yolo_weights_path=None,yolo_config_path= None, yolo_name_path= None, lenet_weights_path=None,save_img=False):
    global user_command
    out, source, weights, view_img, save_txt, imgsz, cfg, names = \
        opt.output, opt.source, opt.weights, opt.view_img, opt.save_txt, opt.img_size, opt.cfg, opt.names
    webcam = source == '0' or source.startswith('rtsp') or source.startswith('http') or source.endswith('.txt')

    if not yolo_weights_path:
        raise FileNotFoundError

    if not lenet_weights_path:
        raise FileNotFoundError

    lenet_model = load_model(lenet_weights_path)
    # BOW = pickle.load(open(BOW_file, 'rb'))
    # Initialize
    # device = select_device(opt.device)
    device = select_device('cpu')
    if os.path.exists(out):
        shutil.rmtree(out)  # delete output folder
    os.makedirs(out)  # make new output folder
    half = device.type != 'cpu'  # half precision only supported on CUDA

    # Load model
    model = Darknet(yolo_config_path).to(device)
    try:
        model.load_state_dict(torch.load(yolo_weights_path, map_location=device)['model'])
        #model = attempt_load(weights, map_location=device)  # load FP32 model
        #imgsz = check_img_size(imgsz, s=model.stride.max())  # check img_size
    except:
        load_darknet_weights(model, yolo_weights_path)
    model.to(device).eval()
    if half:
        model.half()  # to FP16

    # Second-stage classifier
    classify = False
    if classify:
        modelc = load_classifier(name='resnet101', n=2)  # initialize
        modelc.load_state_dict(torch.load('weights/resnet101.pt', map_location=device)['model'])  # load weights
        modelc.to(device).eval()

    # Set Dataloader
    vid_path, vid_writer = None, None
    if webcam:
        view_img = True
        cudnn.benchmark = True  # set True to speed up constant image size inference
        dataset = LoadStreams(source, img_size=imgsz)
    else:
        save_img = True
        dataset = LoadImages(source, img_size=imgsz, auto_size=64)

    # Get names and colors
    names = load_classes(yolo_name_path)
    colors = [[random.randint(0, 255) for _ in range(3)] for _ in range(len(names))]

    # Run inference
    t0 = time.time()
    img = torch.zeros((1, 3, imgsz, imgsz), device=device)  # init img
    _ = model(img.half() if half else img) if device.type != 'cpu' else None  # run once
    for path, img, im0s, vid_cap in dataset:

        # make clean copy of image
        # print(type(im0s))
        im0s_clean = np.array(im0s[0].copy())
        # print(im0s_clean.shape)

        img = torch.from_numpy(img).to(device)
        img = img.half() if half else img.float()  # uint8 to fp16/32
        img /= 255.0  # 0 - 255 to 0.0 - 1.0
        if img.ndimension() == 3:
            img = img.unsqueeze(0)

        # Inference
        t1 = time_synchronized()
        pred = model(img, augment=opt.augment)[0]

        # Apply NMS
        pred = non_max_suppression(pred, opt.conf_thres, opt.iou_thres, classes=opt.classes, agnostic=opt.agnostic_nms)
        t2 = time_synchronized()

        # Apply Classifier
        if classify:
            pred = apply_classifier(pred, modelc, img, im0s)

        # Process detections
        for i, det in enumerate(pred):  # detections per image
            if webcam:  # batch_size >= 1
                p, s, im0 = path[i], '%g: ' % i, im0s[i].copy()
            else:
                p, s, im0 = path, '', im0s

            save_path = str(Path(out) / Path(p).name)
            txt_path = str(Path(out) / Path(p).stem) + ('_%g' % dataset.frame if dataset.mode == 'video' else '')
            s += '%gx%g ' % img.shape[2:]  # print string
            gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh

            bbox_array_msg = BoundingBoxArray()

            if det is not None and len(det):
                # Rescale boxes from img_size to im0 size
                det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()

                # Print results
                for c in det[:, -1].unique():
                    n = (det[:, -1] == c).sum()  # detections per class
                    s += '%g %ss, ' % (n, names[int(c)])  # add to string

                # Write results
                for *xyxy, conf, cls in det:
                    if save_txt:  # Write to file
                        xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh
                        with open(txt_path + '.txt', 'a') as f:
                            f.write(('%g ' * 5 + '\n') % (cls, *xywh))  # label format

                    if save_img or view_img:  # Add bbox to image
                        label = '%s %.2f' % (names[int(cls)], conf)

                        x1 = int(xyxy[0])
                        y1 = int(xyxy[1]) 
                        x2 = int(xyxy[2])
                        y2 = int(xyxy[3])

                        face = im0[y1:y2,x1:x2,:]

                        # print(face.shape)
                        # smile = detect_smile(face, svm_model)
                        smile = detect_smile_lenet(face, lenet_model)

                        bbox_msg = BoundingBox()
                        bbox_msg.probability = conf
                        bbox_msg.xmin = x1
                        bbox_msg.xmax = x2
                        bbox_msg.ymin = y1
                        bbox_msg.ymax = y2
                        bbox_msg.id = len(bbox_array_msg.data)
                        bbox_msg.Class = smile

                        bbox_array_msg.data.append(bbox_msg)

                
                        # plot_one_box(xyxy, im0, label=smile, color=colors[int(cls)], line_thickness=3)

            # Print time (inference + NMS)
            print('%sDone. (%.3fs)' % (s, t2 - t1))

            clean_img_pub.publish(numpy_to_image(im0s_clean,'bgr8'))
            smiling_faces_bbox_pub.publish(bbox_array_msg)
            # Stream results
            if (user_command == 'q') or (cv2.waitKey(1) == ord('q')):
                cv2.destroyAllWindows()
                # camera.release
                # vid_cap.re
                # raise StopIteration
                # raise rospy.ROSInterruptException
                return
                

    print('Done. (%.3fs)' % (time.time() - t0))

def user_input_callback(msg):
    global user_command
    user_command = msg.data


if __name__ == '__main__':

    yolo_weights_path = os.path.join(os.path.dirname(__file__), 'weights', 'best_ap.pt')
    yolo_config_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..", "..", "..",'widerConfig','yolov4-tiny-custom.cfg'))
    yolo_name_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..", "..", "..",'widerConfig','wider.names'))
    lenet_weights_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..", "..", "..",'smile_detection','trained_lenet.hdf5'))
        
    try:
        # initialise node
        rospy.init_node('smiling_face_detector')
        print("[NODE] smiling_face_detector init")
        parser = argparse.ArgumentParser()
        parser.add_argument('--weights', nargs='+', type=str, default='./weights/best_ap.pt', help='model.pt path(s)')
        parser.add_argument('--source', type=str, default='0', help='source')  # file/folder, 0 for webcam
        parser.add_argument('--output', type=str, default='inference/output', help='output folder')  # output folder
        parser.add_argument('--img-size', type=int, default=512, help='inference size (pixels)')
        parser.add_argument('--conf-thres', type=float, default=0.4, help='object confidence threshold')
        parser.add_argument('--iou-thres', type=float, default=0.5, help='IOU threshold for NMS')
        parser.add_argument('--device', default='cpu', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
        parser.add_argument('--view-img', action='store_true', help='display results')
        parser.add_argument('--save-txt', action='store_true', help='save results to *.txt')
        parser.add_argument('--classes', nargs='+', type=int, help='filter by class: --class 0, or --class 0 2 3')
        parser.add_argument('--agnostic-nms', action='store_true', help='class-agnostic NMS')
        parser.add_argument('--augment', action='store_true', help='augmented inference')
        parser.add_argument('--update', action='store_true', help='update all models')
        parser.add_argument('--cfg', type=str, default='/home/evan/mp-picture-perfect/widerConfig/yolov4-tiny-custom.cfg', help='*.cfg path')
        parser.add_argument('--names', type=str, default='/home/evan/mp-picture-perfect/widerConfig/wider.names', help='*.cfg path')
        parser.add_argument('__name:', type=str, default='', help='*.cfg path')
        parser.add_argument('__log:', type=str, default='', help='*.cfg path')
        opt = parser.parse_args()
        print(opt)

        user_command = False
        smiling_faces_bbox_pub = rospy.Publisher('smiling_faces_boxes', BoundingBoxArray, queue_size=20)
        clean_img_pub = rospy.Publisher('clean_stream', rosImage, queue_size=10)
        user_command_sub = rospy.Subscriber('/user_input', rosString, user_input_callback)

        with torch.no_grad():
            if opt.update:  # update all models (to fix SourceChangeWarning)
                for opt.weights in ['']:
                    detect()
                    strip_optimizer(opt.weights)
            else:
                detect(yolo_weights_path, yolo_config_path, yolo_name_path, lenet_weights_path)
        # run node
        # rospy.spin()
        # raise rospy.ROSInterruptException

    except rospy.ROSInterruptException: pass


