#!/usr/bin/env python
import rospy
from std_msgs.msg import String

def user_input():
    pub = rospy.Publisher('user_input', String, queue_size=10)
    rospy.init_node('user_interface')

    while not rospy.is_shutdown():
        
        input_char = input("Select an option:\n[c] clear hats\n[q] quit picture-perfect\n[s] save picture\n[r] retake picture\n")

        while input_char not in ['c', 'q', 'r', 's', 'h']:
            print("Incorrect selection")
            input_char = input("Select an option:\n[c] clear hats\n[q] quit picture-perfect\n[r] retake picture\n")

        print("{} selected.".format(input_char))
        if input_char == 'c':
            print("Clearing current hats")
        elif input_char == 'q':
            print("Quitting picture-perfect")
        elif input_char == 'r':
            print("Retaking photo")
        elif input_char == 's':
            print("Saving current photo")
        elif input_char == 'h':
            drink = input("Select a drink filter:\n")
            while drink not in ['Whisky', 'Tequila', 'Vodka', 'Flying', 'q']:
                print("Invalid drink name")
                drink = input("Select a drink filter:\n")
            input_char = drink

        pub.publish(input_char)

        if input_char == 'q':
            break

if __name__ == '__main__':
    try:
        user_input()
    except rospy.ROSInterruptException:
        pass