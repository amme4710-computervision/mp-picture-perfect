#!/usr/bin/env python3

import rospy
import cv2
import os 
from sensor_msgs.msg import Image as rosImage
from std_msgs.msg import String as rosString 
from pytorch_yolov4.msg import BoundingBox, BoundingBoxArray
from pytorch_yolov4.np2ros import *
import random
import numpy as np
import matplotlib.pyplot as plt
import random

# Class heirachy for image filters
# Example: create a filter by calling:
#   img_filter = MAGAFilter("path-to-filter-overlay")
# apply the filter using (to be implemented):
#   filtered_image = img_filter(image=image, smiling_bboxes=bboxes)

class Person(object):
    def __init__(self, mid, colour):
        self.mid = mid
        self.missed = 0
        self.colour = colour

class ImageFilter(object):
    def __init__(self, path_to_overlay_image, scale, y_shift):
        self.overlay_image = cv2.imread(path_to_overlay_image, cv2.IMREAD_UNCHANGED)
        self.scales = scale
        self.y_shift = y_shift
    
    def overlay(self, image, smiling_bboxes):

        largeImage = image
        overlay_aspect_ratio = self.overlay_image.shape[0]/self.overlay_image.shape[1]
        overlay_scale = self.scales
        h,w,_ = image.shape

        # Go through each bounding box
        for box in smiling_bboxes:

            # Resize image
            midX = box[0]

            size = box[2]
            print("size of bbox:" , size)

            midY = min((box[1] + self.y_shift*size),h)

            if midY < int(h/2):
                top = 1
            else:
                top = 0

            side = box[3]

            x_size = int(overlay_scale*size)
            y_size = int(overlay_aspect_ratio*overlay_scale*size)

            smallImage = cv2.resize(self.overlay_image, (x_size, y_size))

            print('SIZE OF RESIZED IMAGE')
            print(smallImage.shape)

            xStart = int(midX - x_size/2)
            xStart = max(xStart, 0)
            xEnd = int(midX + x_size/2)
            xEnd = min(xEnd, w)

            yStart = int(midY - y_size/2)
            yStart = max(yStart,0)
            yEnd = int(midY + y_size/2)
            yEnd = min(max(yEnd,0), h)

            if side == 1 and top == 1:
                smallImage = smallImage[(y_size - (yEnd-yStart)):, 0:(xEnd-xStart),:]
                print('joe')
                print(xStart, xEnd, yStart, yEnd)
            elif side == 0 and top == 1:
                smallImage = smallImage[(y_size - (yEnd-yStart)):, (x_size - (xEnd-xStart)):,:]
                print('mama')
                print(xStart, xEnd, yStart, yEnd)
            elif side == 1 and top == 0:
                smallImage = smallImage[0:(yEnd-yStart), 0:(xEnd-xStart),:]
                print('so')
                print(xStart, xEnd, yStart, yEnd)
            elif side == 0 and top == 0:
                smallImage = smallImage[0:(yEnd-yStart), (x_size - (xEnd-xStart)):,:]
                print('fat')
                print(xStart, xEnd, yStart, yEnd)

            print(smallImage.shape)
            print(largeImage[yStart:yEnd, xStart:xEnd].shape)
            # Slice and overlay

            try:
                largeImage[yStart:yEnd, xStart:xEnd] = largeImage[yStart:yEnd, xStart:xEnd] * (1 - smallImage[:, :, 3:] / 255) + \
                        smallImage[:, :, :3] * (smallImage[:, :, 3:] / 255)
            except:
                print("Overlay fucked up")
                pass
            
        return largeImage


# image_filter = CossackFilter("path-to-image")
# image = image_filter(image,bbox)

class PicturePerfect(object):
    def __init__(self):

        self.image_sub = rospy.Subscriber('/clean_stream', rosImage, self.image_callback)
        self.smiling_faces_bbox_sub = rospy.Subscriber('/smiling_faces_boxes', BoundingBoxArray, self.smiling_faces_bbox_callback)
        self.drink_detection_sub = rospy.Subscriber('/drinks', BoundingBox, self.drink_detection_callback)
        self.user_command_sub = rospy.Subscriber('/user_input', rosString, self.user_input_callback)
        self.current_frame = None
        self.smiling_bboxes = None
        self.command = None
        self.photo = None
        self.photo_count = 0
        self.person_array = dict()
        self.person_count = 0
        # self.fourcc = cv2.VideoWriter_fourcc(*'MP4V')
        # self.vid_writer = cv2.VideoWriter('/home/evan/mp-picture-perfect/output_images/test.mp4',self.fourcc, 100, (640,480))

        cmap = plt.get_cmap('tab20b')
        self.colours_array = [cmap(i)[:3] for i in np.linspace(0, 1, 20)]

        # self.sombrero_filter = ImageFilter(os.path.abspath(os.path.join(os.path.dirname(__file__), 
        #                                     "..", "..", "..", "..",
        #                                     'overlays',
        #                                     'sombrero_red.png')),3.2, 30) # change path
                                            
        # self.maga_filter = ImageFilter(os.path.abspath(os.path.join(os.path.dirname(__file__), 
        #                                     "..", "..", "..", "..",
        #                                     'overlays',
        #                                     'magahat3.png')), 1.2, 80)
        # self.ushanka_filter = ImageFilter(os.path.abspath(os.path.join(os.path.dirname(__file__), 
        #                                     "..", "..", "..", "..",
        #                                     'overlays',
        #                                     'ushanka.png')), 1.5, 250)
        # self.sayan_filter = ImageFilter(os.path.abspath(os.path.join(os.path.dirname(__file__), 
        #                                     "..", "..", "..", "..",
        #                                     'overlays',
        #                                     'sayan.png')),  2, 60)


        self.sombrero_filter = ImageFilter(os.path.abspath(os.path.join(os.path.dirname(__file__), 
                                            "..", "..", "..", "..",
                                            'overlays',
                                            'sombrero_red.png')),3.0, -0.2) # change path
                                            
        self.maga_filter = ImageFilter(os.path.abspath(os.path.join(os.path.dirname(__file__), 
                                            "..", "..", "..", "..",
                                            'overlays',
                                            'magahat3.png')), 0.95, 0)
        self.ushanka_filter = ImageFilter(os.path.abspath(os.path.join(os.path.dirname(__file__), 
                                            "..", "..", "..", "..",
                                            'overlays',
                                            'ushanka.png')), 1.5, 0.6)
        self.sayan_filter = ImageFilter(os.path.abspath(os.path.join(os.path.dirname(__file__), 
                                            "..", "..", "..", "..",
                                            'overlays',
                                            'sayan.png')), 2, -0.2)

        self.drink = None
        self.drink_bbox = []

    def image_callback(self, msg):
        self.current_frame = image_to_numpy(msg)
        print(self.current_frame.shape)

    def smiling_faces_bbox_callback(self, msg):
        self.smiling_bboxes = msg.data

    def drink_detection_callback(self,msg):
        self.drink = msg.Class

        self.drink_bbox = [msg.xmin, msg.ymin, msg.xmax, msg.ymax]

    def user_input_callback(self, msg):
        self.command = msg.data
        if self.command == 'c':
            self.drink = None

        elif self.command == 'r':
            self.photo = None

        elif self.command == 's':
            if self.photo is None:
                print("Cannot save photo, please take a photo first")
            else:

                out_image_dir =  os.path.abspath(os.path.join(os.path.dirname(__file__), 
                                            "..", "..", "..", "..",
                                            'output_images'))
                filenames = os.listdir(out_image_dir)
                count  = len(filenames)
                out_image_path = os.path.join(out_image_dir, "Photobooth" + str(count) + ".jpg")
                cv2.imwrite(out_image_path, self.photo)
                self.photo_count += 1

        elif len(msg.data) > 1:
            self.drink = msg.data
        # if (msg.data == 'q'):
        #     cv2.destroyAllWindows()
        #     raise rospy.ROSInterruptException


    def process_data(self):
        
        if self.current_frame is None:
            return

        # clean_image = self.current_frame.copy()
        # clean_image = cv2.flip(clean_image,1)
        detections = self.current_frame.copy()
        hat_image = self.current_frame.copy()

        value = "nil"
        colour = (0, 0, 0)

        if self.smiling_bboxes is not None:

            faces = []
            smile_count = 0
            # detections = self.current_frame.copy()
            matched_list = []
            
            for bbox in self.smiling_bboxes:
                xyxy = [bbox.xmin, bbox.ymin, bbox.xmax, bbox.ymax]
                smile = bbox.Class

                width = bbox.xmax - bbox.xmin
                height = bbox.ymax - bbox.ymin
                size = max(width, height)
                midX = int(bbox.xmin + (bbox.xmax-bbox.xmin)/2)
                midY = int(bbox.ymin + (bbox.ymax-bbox.ymin)/2)
                mid = [midX,midY]

                if bbox.xmin < detections.shape[1]//2:
                    face_info = [midX, bbox.ymin, size, 0]
                else:
                    face_info = [midX, bbox.ymin, size, 1]

                faces.append(face_info)

                if bbox.Class == 'smile':
                    smile_count = smile_count + 1

                if list(self.person_array.keys()):
                    for key,person in self.person_array.items():
                        if np.linalg.norm(np.array(person.mid) - np.array(mid)) < 40:
                            person.mid = mid
                            value = key
                            colour = person.colour
                            # matched = True
                            matched_list.append(key)
                            break

                    if len(matched_list) < 1:
                        self.person_array[self.person_count] = Person(mid, self.colours_array[self.person_count])
                        self.person_count+= 1

                else:
                    self.person_array[self.person_count] = Person(mid, self.colours_array[self.person_count])
                    matched_list.append(self.person_count)
                    self.person_count+= 1
                    value = self.person_count
                    colour = self.colours_array[self.person_count]

                self.plot_one_box(xyxy, detections, label=smile+" "+str(value), colour=colour, line_thickness=3)

            existing = set(list(self.person_array.keys()))
            current = set(matched_list)
            missed = existing.difference(current)

            for ids in missed:
                if self.person_array[ids].missed < 10:
                    self.person_array[ids].missed += 1
                else:
                    del self.person_array[ids]

            # if there is a drink detected in the image, overlay relevant hats
            if self.drink is not None:
                print('Faces')
                print(faces)
                if self.drink == 'Tequila':
                    hat_image = self.sombrero_filter.overlay(hat_image, faces)
                    # detections = self.sombrero_filter.overlay(detections, faces)
                elif self.drink == 'Whisky':
                    hat_image = self.maga_filter.overlay(hat_image, faces)
                elif self.drink == 'Vodka':
                    hat_image = self.ushanka_filter.overlay(hat_image, faces)
                elif self.drink == 'Flying':
                    hat_image = self.sayan_filter.overlay(hat_image, faces)          

                # self.plot_one_box(self.drink_bbox, clean_image, label=self.drink, colour=colour, line_thickness=3)  
                    
            # if everyone is smiling, and there is no current photo, take a photo
            if (len(self.smiling_bboxes) == smile_count) and (len(self.smiling_bboxes) > 0):
                if self.photo is None:
                    self.photo = hat_image

        # to make sure numbers don't go crazy and colour map always has a value
        if self.person_count >= 19:
            self.person_count = 0

        # self.plot_one_box(self.drink_bbox, detections, label=self.drink, color=(1, 1, 1), line_thickness=3)
        if self.photo is not None:
            cv2.imshow('Current Photo', self.photo)

        cv2.imshow('PhotoBooth', hat_image)

        cv2.imshow('Detections', detections)

        # self.vid_writer.write(detections)

        
            
        # cv2.imshow('EAST + Tesseract Text Detection', clean_image)
        cv2.waitKey(1)
        # if cv2.waitKey(1) == ord('q'):
        #     raise rospy.ROSInterruptException

    # def scale_overlay(self, bbox, overlay):
        


    def plot_one_box(self, x, img, colour=None, label=None, line_thickness=None):
        colour_255 = tuple([c * 255 for c in colour])
        print(colour)
        # Plots one bounding box on image img
        tl = line_thickness or round(0.002 * (img.shape[0] + img.shape[1]) / 2) + 1  # line/font thickness
        # color = color or [random.randint(0, 255) for _ in range(3)]
        c1, c2 = (int(x[0]), int(x[1])), (int(x[2]), int(x[3]))
        cv2.rectangle(img, c1, c2, colour_255, thickness=tl, lineType=cv2.LINE_AA)
        if label:
            tf = max(tl - 1, 1)  # font thickness
            t_size = cv2.getTextSize(label, 0, fontScale=tl / 3, thickness=tf)[0]
            c2 = c1[0] + t_size[0], c1[1] - t_size[1] - 3
            cv2.rectangle(img, c1, c2, colour_255, -1, cv2.LINE_AA)  # filled
            cv2.putText(img, label, (c1[0], c1[1] - 2), 0, tl / 3, [225, 255, 255], thickness=tf, lineType=cv2.LINE_AA)

        return img


if __name__ == '__main__':        
    try:
        # initialise node
        rospy.init_node('picture_perfect')
        print("[NODE] picture_perfect init")

        demo = PicturePerfect()
        
        while not rospy.is_shutdown():

            # drink_label_flag = False
            demo.process_data()

            if demo.command is not None and demo.command == 'q':
                cv2.destroyAllWindows()
                # demo.vid_writer.release()
                raise rospy.ROSInterruptException

        # run node
        rospy.spin()

    except rospy.ROSInterruptException: pass