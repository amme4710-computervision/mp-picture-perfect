#!/usr/bin/env python3
# import the necessary packages
from imutils.video import VideoStream
from imutils.video import FPS
from imutils.object_detection import non_max_suppression
from matplotlib.pyplot import xlabel
import numpy as np
import argparse
import imutils
import time
import cv2
import pytesseract
from pytesseract import Output
import matplotlib.pyplot as plt

# ROS Packages
import rospy
from pytorch_yolov4.msg import BoundingBox
from pytorch_yolov4.msg import BoundingBoxArray
from sensor_msgs.msg import Image as RosImage
from pytorch_yolov4.np2ros import *

class CroppedImage:

	def __init__(self, cropped, xmin, xmax, ymin, ymax):
		self.cropped = cropped
		self.xmin = xmin
		self.xmax = xmax
		self.ymin = ymin
		self.ymax = ymax

	def getImage(self):
		return self.cropped

	def getDimensions(self):
		return (self.xmin, self.xmax, self.ymin, self.ymax)

def imageCallback(msg):

	# Convert a ros image to an actual image for open cv
	image = image_to_numpy(msg)

	# ROS - publishing message	
	publishingMessage = BoundingBox()
	publishingMessage.probability = 1
	publishingMessage.xmin = 0
	publishingMessage.xmax = 0
	publishingMessage.ymin = 0
	publishingMessage.ymax = 0
	publishingMessage.id = 0
	publishingMessage.String = "No drink detected"



def decode_predictions(scores, geometry):

	# grab the number of rows and columns from the scores volume, then
	# initialize our set of bounding box rectangles and corresponding
	# confidence scores
	(numRows, numCols) = scores.shape[2:4]
	rects = []
	confidences = []

	# loop over the number of rows
	for y in range(0, numRows):

		# extract the scores (probabilities), followed by the
		# geometrical data used to derive potential bounding box
		# coordinates that surround text
		scoresData = scores[0, 0, y]
		xData0 = geometry[0, 0, y]
		xData1 = geometry[0, 1, y]
		xData2 = geometry[0, 2, y]
		xData3 = geometry[0, 3, y]
		anglesData = geometry[0, 4, y]

		# loop over the number of columns
		for x in range(0, numCols):

			# if our score does not have sufficient probability, ignore it
			if scoresData[x] < args["min_confidence"]:
				continue

			# compute the offset factor as our resulting feature maps will be 4x smaller than the input image
			(offsetX, offsetY) = (x * 4.0, y * 4.0)

			# extract the rotation angle for the prediction and then compute the sin and cosine
			angle = anglesData[x]
			cos = np.cos(angle)
			sin = np.sin(angle)

			# use the geometry volume to derive the width and height of the bounding box
			h = xData0[x] + xData2[x]
			w = xData1[x] + xData3[x]

			# compute both the starting and ending (x, y)-coordinates for the text prediction bounding box
			endX = int(offsetX + (cos * xData1[x]) + (sin * xData2[x]))
			endY = int(offsetY - (sin * xData1[x]) + (cos * xData2[x]))
			startX = int(endX - w)
			startY = int(endY - h)

			# add the bounding box coordinates and probability score to our respective lists
			rects.append((startX, startY, endX, endY))
			confidences.append(scoresData[x])

	# return a tuple of the bounding boxes and associated confidences
	return (rects, confidences)

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-east", "--east", type=str, required=True,
	help="path to input EAST text detector")
ap.add_argument("-v", "--video", type=str,
	help="path to optinal input video file")
ap.add_argument("-c", "--min-confidence", type=float, default=0.5,
	help="minimum probability required to inspect a region")
ap.add_argument("-w", "--width", type=int, default=320,
	help="resized image width (should be multiple of 32)")
ap.add_argument("-e", "--height", type=int, default=320,
	help="resized image height (should be multiple of 32)")
args = vars(ap.parse_args())

# initialize the original frame dimensions, new frame dimensions, and ratio between the dimensions
(W, H) = (None, None)
(newW, newH) = (args["width"], args["height"])
(rW, rH) = (None, None)

# define the two output layer names for the EAST detector model
layerNames = [
	"feature_fusion/Conv_7/Sigmoid",
	"feature_fusion/concat_3"]

# load the pre-trained EAST text detector
print("[INFO] loading EAST text detector...")
net = cv2.dnn.readNet(args["east"])

# if a video path was not supplied, grab the reference to the web cam
if not args.get("video", False):
	print("[INFO] starting video stream...")
	vs = VideoStream(src=0).start()
	time.sleep(1.0)

# otherwise, grab a reference to the video file
else:
	vs = cv2.VideoCapture(args["video"])

# start the FPS throughput estimator
fps = FPS().start()


saiyan = cv2.imread("/home/khit/Uni/AMME4710/Major/saiyanHair.png")
saiyan = cv2.resize(saiyan, (150, 150))
graySaiyan = cv2.cvtColor(saiyan, cv2.COLOR_BGR2GRAY)
myMask = cv2.bitwise_not(graySaiyan)
foreground = cv2.bitwise_or(saiyan, saiyan, mask=myMask)
print("foreground shape")
print(foreground.shape)

hat = cv2.imread("/home/khit/Uni/AMME4710/Major/sombrero.png")
hat = cv2.resize(hat, (150, 150))
grayHat = cv2.cvtColor(hat, cv2.COLOR_BGR2GRAY)
hatMask = cv2.bitwise_not(grayHat)
hatForeground = cv2.bitwise_or(hat, hat, mask=hatMask)

flying = False
mexican = False

#-----------------------------------------------------------------------
# ROS

# Create a node
rospy.init_node("alcohol_detection")

# Create a publisher
pub = rospy.Publisher("drinks", BoundingBox, queue_size=10)

# Create a subscriber
sub = rospy.Subscriber("/clean_stream", RosImage, imageCallback)

#-----------------------------------------------------------------------

# loop over frames from the video stream
while True:	

	# grab the current frame, then handle if we are using a VideoStream or VideoCapture object
	frame = vs.read()
	frame = frame[1] if args.get("video", False) else frame

	# check to see if we have reached the end of the stream
	if frame is None:
		break

	# resize the frame, maintaining the aspect ratio
	frame = imutils.resize(frame, width=500)
	orig = frame.copy()

	roi = orig[25:175, 200:350]

	# if our frame dimensions are None, we still need to compute the
	# ratio of old frame dimensions to new frame dimensions
	if W is None or H is None:
		(H, W) = frame.shape[:2]
		rW = W / float(newW)
		rH = H / float(newH)

	# resize the frame, this time ignoring aspect ratio
	frame = cv2.resize(frame, (newW, newH))

	# construct a blob from the frame and then perform a forward pass
	# of the model to obtain the two output layer sets
	blob = cv2.dnn.blobFromImage(frame, 1.0, (newW, newH),
		(123.68, 116.78, 103.94), swapRB=True, crop=False)
	net.setInput(blob)
	(scores, geometry) = net.forward(layerNames)

	# decode the predictions, then  apply non-maxima suppression to
	# suppress weak, overlapping bounding boxes
	(rects, confidences) = decode_predictions(scores, geometry)
	boxes = non_max_suppression(np.array(rects), probs=confidences)

	images = []

	# loop over the bounding boxes
	for (startX, startY, endX, endY) in boxes:

		# scale the bounding box coordinates based on the respective ratios
		startX = int(startX * rW)
		startY = int(startY * rH)
		endX = int(endX * rW)
		endY = int(endY * rH)

		xOffset = endX - startX
		yOffset = endY - startY

		xOffset = int(xOffset / 5)
		yOffset = int(yOffset / 5)

		croppedImage = orig[startY-yOffset : endY+yOffset, startX-xOffset : endX+xOffset]
		croppedImageObj = CroppedImage(croppedImage, startX-xOffset, endX+xOffset, startY-yOffset, endY+yOffset)
		images.append(croppedImageObj)

		# draw the bounding box on the frame
		cv2.rectangle(orig, (startX, startY), (endX, endY), (0, 255, 0), 2)

	for imageObj in images:

		# Get cropped image
		cropped = imageObj.getImage()

		try:
			cropped = cv2.cvtColor(cropped, cv2.COLOR_RGB2GRAY)
		except Exception as e:
			continue

		detectedString = pytesseract.image_to_string(cropped)
		print(detectedString)

		if "WHISKY" in detectedString:
			print("Whisky detected!")

		elif "FLY" in detectedString or "YIN" in detectedString:
			flying = True
			print("Flying Power detected!")

		elif "TEQUILA" in detectedString or "SIERRA" in detectedString:	

			# Establish class
			publishingMessage.String = "Tequila"

			# Fill in dimensions
			(xMin, xMax, yMin, yMax) = imageObj.getDimensions()
			publishingMessage.xmin = xMin
			publishingMessage.xmax = xMax
			publishingMessage.ymin = yMin
			publishingMessage.ymax = yMax

			mexican = True
			print("Tequila detected")

	# update the FPS counter
	fps.update()

	# ROS - publish
	pub.publish(publishingMessage)

	if flying == True:
		finalROI = cv2.bitwise_or(roi, foreground)

		largeImage = orig
		smallImage = finalROI

		largeImage[25:175, 200:350] = smallImage

		# show the output frame
		cv2.imshow("Text Detection", largeImage)
		key = cv2.waitKey(1) & 0xFF

	elif mexican == True:
		finalROI = cv2.bitwise_or(roi, hatForeground)

		largeImage = orig
		smallImage = finalROI

		largeImage[25:175, 200:350] = smallImage

		# show the output frame
		cv2.imshow("Text Detection", largeImage)
		key = cv2.waitKey(1) & 0xFF

	else:
		cv2.imshow("Text Detection", orig)
		key = cv2.waitKey(1) & 0xFF

	# if the `q` key was pressed, break from the loop
	if key == ord("q"):
		break

# stop the timer and display FPS information
fps.stop()
print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

# if we are using a webcam, release the pointer
if not args.get("video", False):
	vs.stop()

# otherwise, release the file pointer
else:
	vs.release()

# close all windows
cv2.destroyAllWindows()