#!/usr/bin/env python3

from imutils.video import VideoStream
from imutils.video import FPS
from imutils.object_detection import non_max_suppression
from matplotlib.pyplot import xlabel
import numpy as np
import argparse
import imutils
import time
import cv2
import pytesseract
from pytesseract import Output
import matplotlib.pyplot as plt
import rospy
from rospy.exceptions import ROSInterruptException
from sensor_msgs.msg import Image as rosImage
from pytorch_yolov4.msg import BoundingBox, BoundingBoxArray
from pytorch_yolov4.np2ros import *
from std_msgs.msg import String as rosString
import os

class CroppedImage:

	def __init__(self, cropped, xmin, xmax, ymin, ymax):
		self.cropped = cropped
		self.xmin = xmin
		self.xmax = xmax
		self.ymin = ymin
		self.ymax = ymax

	def getImage(self):
		return self.cropped

	def getDimensions(self):
		return (self.xmin, self.xmax, self.ymin, self.ymax)

class DrinkDetector:

    def __init__(self, east_net_path):

        (W, H) = (None, None)
        (newW, newH) = (320, 320)
        (rW, rH) = (None, None)

        self.W = W
        self.H = H

        self.count = 0

        self.stop = False

        self.eastNet = cv2.dnn.readNet(east_net_path)

        self.user_command_sub = rospy.Subscriber('/user_input', rosString, self.user_input_callback)

        # define the two output layer names for the EAST detector model
        self.layerNames = [
            "feature_fusion/Conv_7/Sigmoid",
            "feature_fusion/concat_3"]

        self.image_sub = rospy.Subscriber("/clean_stream", rosImage, self.image_callback)
        self.detection_pub = rospy.Publisher("drinks", BoundingBox, queue_size=10)

    def user_input_callback(self, msg):
        if msg.data == 'q':
            self.stop = True

    def decode_predictions(self, scores, geometry):

        # grab the number of rows and columns from the scores volume, then
        # initialize our set of bounding box rectangles and corresponding
        # confidence scores
        (numRows, numCols) = scores.shape[2:4]
        rects = []
        confidences = []

        # loop over the number of rows
        for y in range(0, numRows):

            # extract the scores (probabilities), followed by the
            # geometrical data used to derive potential bounding box
            # coordinates that surround text
            scoresData = scores[0, 0, y]
            xData0 = geometry[0, 0, y]
            xData1 = geometry[0, 1, y]
            xData2 = geometry[0, 2, y]
            xData3 = geometry[0, 3, y]
            anglesData = geometry[0, 4, y]

            # loop over the number of columns
            for x in range(0, numCols):

                # if our score does not have sufficient probability, ignore it
                if scoresData[x] < 0.5:
                    continue

                # compute the offset factor as our resulting feature maps will be 4x smaller than the input image
                (offsetX, offsetY) = (x * 4.0, y * 4.0)

                # extract the rotation angle for the prediction and then compute the sin and cosine
                angle = anglesData[x]
                cos = np.cos(angle)
                sin = np.sin(angle)

                # use the geometry volume to derive the width and height of the bounding box
                h = xData0[x] + xData2[x]
                w = xData1[x] + xData3[x]

                # compute both the starting and ending (x, y)-coordinates for the text prediction bounding box
                endX = int(offsetX + (cos * xData1[x]) + (sin * xData2[x]))
                endY = int(offsetY - (sin * xData1[x]) + (cos * xData2[x]))
                startX = int(endX - w)
                startY = int(endY - h)

                # add the bounding box coordinates and probability score to our respective lists
                rects.append((startX, startY, endX, endY))
                confidences.append(scoresData[x])

        # return a tuple of the bounding boxes and associated confidences
        return (rects, confidences)

    def image_callback(self, msg):

        # global W
        # global H
        # global net
        # global layerNames
        if self.count < 4:
            self.count += 1
            return 

        self.count = 0

        image = image_to_numpy(msg)
        image = cv2.flip(image,1)
        orig = image.copy()

        resized_image = imutils.resize(image, width=500)
        resized_image_copy = resized_image.copy()
        
        roi = resized_image[25:175, 200:350]

        W = self.W
        H = self.H

        newW = 320
        newH = 320

        # if our resized_image dimensions are None, we still need to compute the
        # ratio of old resized_image dimensions to new resized_image dimensions
        if W is None or self.H is None:
            (H, W) = resized_image_copy.shape[:2]
            rW = W / float(newW)
            rH = H / float(newH)

        # resize the resized_image, this time ignoring aspect ratio
        resized_image = cv2.resize(resized_image, (newW, newH)) 

        # construct a blob from the resized_image and then perform a forward pass
        # of the model to obtain the two output layer sets
        blob = cv2.dnn.blobFromImage(resized_image, 1.0, (newW, newH),
            (123.68, 116.78, 103.94), swapRB=True, crop=False)
        self.eastNet.setInput(blob)
        (scores, geometry) = self.eastNet.forward(self.layerNames)

        # decode the predictions, then  apply non-maxima suppression to
        # suppress weak, overlapping bounding boxes
        (rects, confidences) = self.decode_predictions(scores, geometry)
        boxes = non_max_suppression(np.array(rects), probs=confidences)

        images = []

        # loop over the bounding boxes
        for (startX, startY, endX, endY) in boxes:

            # scale the bounding box coordinates based on the respective ratios
            startX = int(startX * rW)
            startY = int(startY * rH)
            endX = int(endX * rW)
            endY = int(endY * rH)

            xOffset = endX - startX
            yOffset = endY - startY

            xOffset = int(xOffset / 5)
            yOffset = int(yOffset / 5)

            croppedImage = resized_image_copy[startY-yOffset : endY+yOffset, startX-xOffset : endX+xOffset]
            croppedImageObj = CroppedImage(croppedImage, startX-xOffset, endX+xOffset, startY-yOffset, endY+yOffset)
            images.append(croppedImageObj)

            # draw the bounding box on the resized_image
            cv2.rectangle(resized_image_copy, (startX, startY), (endX, endY), (0, 255, 0), 2)

        for imageObj in images:

            # Get cropped image
            cropped = imageObj.getImage()

            try:
                cropped = cv2.cvtColor(cropped, cv2.COLOR_RGB2GRAY)
            except Exception as e:
                continue

            detectedString = pytesseract.image_to_string(cropped)
            print(detectedString)

            whitelist = set('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
            detectedString = ''.join(filter(whitelist.__contains__, detectedString))



            if len(detectedString) < 3:
                continue

            if "ABSOLUT" in detectedString or "LIME" in detectedString:
                print("Vodka detected!")
                publishingMessage = BoundingBox()
                # Establish class
                publishingMessage.Class = "Vodka"

                # Fill in dimensions
                (xMin, xMax, yMin, yMax) = imageObj.getDimensions()
                publishingMessage.xmin = xMin
                publishingMessage.xmax = xMax
                publishingMessage.ymin = yMin
                publishingMessage.ymax = yMax

                self.detection_pub.publish(publishingMessage)

            elif "FLY" in detectedString or "ING" in detectedString:
                print("Flying Power detected!")

                publishingMessage = BoundingBox()
                # Establish class
                publishingMessage.Class = "Flying"

                # Fill in dimensions
                (xMin, xMax, yMin, yMax) = imageObj.getDimensions()
                publishingMessage.xmin = xMin
                publishingMessage.xmax = xMax
                publishingMessage.ymin = yMin
                publishingMessage.ymax = yMax

                self.detection_pub.publish(publishingMessage)

            elif "EQUIL" in detectedString or "IERR" in detectedString:	
                print("Tequila detected!")

                publishingMessage = BoundingBox()
                # Establish class
                publishingMessage.Class = "Tequila"

                # Fill in dimensions
                (xMin, xMax, yMin, yMax) = imageObj.getDimensions()
                publishingMessage.xmin = xMin
                publishingMessage.xmax = xMax
                publishingMessage.ymin = yMin
                publishingMessage.ymax = yMax

                self.detection_pub.publish(publishingMessage)

            elif "Makers" in detectedString or "Mark" in detectedString or "WHISKY" in detectedString:
                print("Whisky detected!")

                publishingMessage = BoundingBox()
                # Establish class
                publishingMessage.Class = "Whisky"

                # Fill in dimensions
                (xMin, xMax, yMin, yMax) = imageObj.getDimensions()
                publishingMessage.xmin = xMin
                publishingMessage.xmax = xMax
                publishingMessage.ymin = yMin
                publishingMessage.ymax = yMax

                self.detection_pub.publish(publishingMessage)

            # # update the FPS counter
            # fps.update()

            # ROS - publish
            
        # cv2.imshow("", orig)
        # cv2.waitKey(1)
        # plt.imshow(orig)
        # plt.show()


if __name__ == '__main__':

    east_net_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "frozen_east_text_detection.pb"))
    try:
        # initialise node
        rospy.init_node('drink_detection')
        print("[NODE] drink_detection init")

        drink_detector = DrinkDetector(east_net_path)
        
        while not drink_detector.stop:
            pass

    except rospy.ROSInterruptException: 
        pass
