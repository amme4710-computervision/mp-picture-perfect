#!/bin/bash
read -p 'Create Conda Environment [mp-picture-perfect] (y/n)?: ' input

if [ "$input" == "y" ]
then

    conda create --name mp-picture-perfect python=3.8 pip --file requirements_old.txt -c conda-forge
    
fi
